﻿using System;
using System.Linq;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Windows.UI.Popups;

namespace DragAndDropOnto
{
    public class TestItem : ViewModelBase
    {
        public string Name { get; set; }

        private ICommand _updateDragFeedbackCommand;
        public ICommand UpdateDragFeedbackCommand
        {
            get
            {
                return _updateDragFeedbackCommand ??
                       (_updateDragFeedbackCommand = new RelayCommand<bool>(OnUpdateDragFeedback));
            }
        }

        private ICommand _dataDroppedCommand;
        public ICommand DataDroppedCommand
        {
            get { return _dataDroppedCommand ?? (_dataDroppedCommand = new RelayCommand<DragDropCollection>(OnDataDropped)); }
        }

        private async void OnDataDropped(DragDropCollection collection)
        {
            var itemsString = "Dropping ";
            foreach (var item in collection.Items.OfType<TestItem>())
            {
                itemsString += item.Name;
                itemsString += ", ";
            }

            var messageDialog = new MessageDialog(string.Format("{0} onto {1}", itemsString, Name));
            await messageDialog.ShowAsync();
        }

        private void OnUpdateDragFeedback(bool isDragOver)
        {
            IsDragOver = isDragOver;
        }

        private bool _isDragOver;
        public bool IsDragOver
        {
            get { return _isDragOver; }
            set
            {
                if (value == _isDragOver)
                    return;

                _isDragOver = value;
                RaisePropertyChanged("IsDragOver");
            }
        }
    }
}
