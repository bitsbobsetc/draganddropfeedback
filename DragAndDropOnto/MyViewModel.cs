﻿using System.Collections.ObjectModel;
using GalaSoft.MvvmLight;

namespace DragAndDropOnto
{
    public class MyViewModel : ViewModelBase
    {
        public ObservableCollection<TestItem> Items { get; set; }
        public ObservableCollection<TestItem> ContainerCollection { get; set; }

        public MyViewModel()
        {
            Items = new ObservableCollection<TestItem>();
            ContainerCollection = new ObservableCollection<TestItem>();

            for (var i = 0; i < 8; i++)
                Items.Add(new TestItem { Name = string.Format("Item {0}", i) });

            for (var i = 0; i < 5; i++)
                ContainerCollection.Add(new TestItem { Name = string.Format("Collection {0}", i) });
        }
    }
}
