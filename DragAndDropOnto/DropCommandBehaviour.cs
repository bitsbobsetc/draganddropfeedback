﻿using System.Collections;
using System.Windows.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace DragAndDropOnto
{
    /// <summary>
    /// Defines a class for managing a collection of dragged items
    /// </summary>
    public class DragDropCollection
    {
        public DragDropCollection(IEnumerable items)
        {
            Items = items;
        }

        public IEnumerable Items { get; set; }
    }

    /// <summary>
    /// Behaviour responsible for managing dropping items
    /// </summary>
    public class DropCommandBehaviour
    {
        private readonly FrameworkElement _targetElement;

        public static readonly DependencyProperty CommandProperty =
            DependencyProperty.RegisterAttached(
                "Command", typeof(ICommand), typeof(DropCommandBehaviour),
                new PropertyMetadata(null, OnCommandChanged));

        public static ICommand GetCommand(UIElement control)
        {
            return control.GetValue(CommandProperty) as ICommand;
        }

        public static void SetCommand(UIElement control, ICommand command)
        {
            control.SetValue(CommandProperty, command);
        }

        /// <summary>
        /// When the command is updated, set the property AttachedDragDropBehaviourProperty on the Border control with
        /// a new instance of this behaviour
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private static void OnCommandChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var border = obj as Border;
            if (border != null)
            {
                border.AllowDrop = true;
                border.SetValue(AttachedDragDropBehaviourProperty, new DropCommandBehaviour(border));
            }
        }

        public static readonly DependencyProperty DragFeedbackCommandProperty =
            DependencyProperty.RegisterAttached(
                "DragFeedbackCommand", typeof(ICommand), typeof(DropCommandBehaviour),
                new PropertyMetadata(null, null));

        public static ICommand GetDragFeedbackCommand(UIElement control)
        {
            return control.GetValue(DragFeedbackCommandProperty) as ICommand;
        }

        public static void SetDragFeedbackCommand(UIElement control, ICommand command)
        {
            control.SetValue(DragFeedbackCommandProperty, command);
        }

        /// <summary>
        /// Attached property to be used with Border control - this allows us to create an instance of the DropCommandBehaviour
        /// class with a reference to the Border control
        /// </summary>
        public static readonly DependencyProperty AttachedDragDropBehaviourProperty =
            DependencyProperty.RegisterAttached(
                "AttachedDragDropBehaviourProperty", typeof(DropCommandBehaviour),
                typeof(DropCommandBehaviour), new PropertyMetadata(null));

        private DropCommandBehaviour(FrameworkElement element)
        {
            _targetElement = element;
            
            _targetElement.DragOver += OnDragOver;
            _targetElement.DragLeave += OnDragLeave;

            DragEventHandler dragHandler = OnDrop;

            _targetElement.RemoveHandler(UIElement.DropEvent, dragHandler);
            _targetElement.AddHandler(UIElement.DropEvent, dragHandler, true);
        }

        private void OnDragOver(object sender, DragEventArgs e)
        {
            UpdateDragFeedback(sender, true);
        }

        private void OnDragLeave(object sender, DragEventArgs e)
        {
            UpdateDragFeedback(sender, false);
        }

        private void OnDrop(object sender, DragEventArgs e)
        {
            var listview = sender as UIElement;
            if (listview == null)
                return;

            var command = listview.GetValue(CommandProperty) as ICommand;
            if (command == null)
                return;

            object obj;
            e.Data.Properties.TryGetValue("DragDropCollection", out obj);

            var dropCollection = obj as DragDropCollection;
            if (dropCollection == null) 
                return;

            command.Execute(dropCollection);

            UpdateDragFeedback(sender, false);
        }

        /// <summary>
        /// Show feedback by executing the Command set on the Command property
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="isDragging"></param>
        private void UpdateDragFeedback(object sender, bool isDragging)
        {
            var element = sender as UIElement;
            if (element != null)
            {
                var command = element.GetValue(DragFeedbackCommandProperty) as ICommand;
                if (command != null)
                    command.Execute(isDragging);
            }
        }
    }
}
